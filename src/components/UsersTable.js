/**
 * Created by Filip Drgoň on 12/03/18.
 */

import React from 'react';
import PropTypes from 'prop-types';
import CustomScroll from "react-custom-scroll";

import UsersTableHeader from "./UsersTableHeader";
import UsersTableRow from "./UsersTableRow";
import CreateNewUser from "./CreateNewUser";

const headers = ["id", "name"];

const UsersTable = ({users, deleteHandler, editHandler, addHandler}) => {
    return (
        <div className="table-component">
            <div className="headers">
                {
                    headers.map(userHeader =>
                        <UsersTableHeader
                            key={userHeader}
                            userHeader={userHeader}
                        />)
                }
            </div>
            <div className="rows">
                <CustomScroll heightRelativeToParent="100%">
                    {
                        users.map(user =>
                            (<UsersTableRow
                                key={user.id}
                                user={user}
                                deleteHandler={deleteHandler}
                                editHandler={editHandler}
                            />))
                    }
                </CustomScroll>
            </div>
            <CreateNewUser
                addHandler={addHandler}
            />
        </div>
    );
};

UsersTable.propTypes = {
    users: PropTypes.arrayOf(PropTypes.object),
    deleteHandler: PropTypes.func,
    editHandler: PropTypes.func,
    addHandler: PropTypes.func,
};

export default UsersTable;