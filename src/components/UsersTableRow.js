/**
 * Created by Filip Drgoň on 12/03/18.
 */

import React, {Component} from 'react';
import PropTypes from 'prop-types';

export default class UsersTableRow extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: props.user.name,
            isEditing: false,
        }
    }

    editUser = () => {
        const {name} = this.state;
        const {editHandler, user} = this.props;
        editHandler({id: user.id, name});
        this.setState({user: "", isEditing: false})
    };

    render() {
        const {name, isEditing} = this.state;
        const {user, deleteHandler} = this.props;
        return (
            <div className="row">
                <div className="cell">
                    {user.id}
                </div>
                <div className="cell">
                    {
                        isEditing ?
                            <input
                                type="text"
                                value={name}
                                onChange={(e) => this.setState({name: e.target.value})}
                                onKeyDown={e => e.keyCode === 13 && this.editUser()}
                                ref={ref => this.input = ref}
                                onFocus={(e) => {
                                    let temp = e.target.value;
                                    e.target.value = '';
                                    e.target.value = temp;
                                }}
                            />
                            :
                            user.name
                    }
                </div>
                <div className="cell controls">
                    <div className="save">
                        {isEditing && <i className="fa fa-check" onClick={this.editUser}/>}
                    </div>
                    <div className="edit">
                        {!isEditing && <i className="fa fa-pencil"
                                          onClick={() => this.setState({isEditing: true}, () => this.input.focus())}/>}
                    </div>
                    <div className="delete">
                        <i className="fa fa-times" onClick={() => deleteHandler(user.id)}/>
                    </div>
                </div>
            </div>
        );
    }
}

UsersTableRow.propTypes = {
    user: PropTypes.object,
    deleteHandler: PropTypes.func,
};