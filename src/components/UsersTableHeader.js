/**
 * Created by Filip Drgoň on 12/03/18.
 */


import React from 'react';
import PropTypes from 'prop-types';

const UsersTableHeader = ({userHeader}) => {
    return (
        <div className="header">
            {userHeader}
        </div>
    );
};

UsersTableHeader.propTypes = {
    userHeader: PropTypes.string,
};

export default UsersTableHeader;
