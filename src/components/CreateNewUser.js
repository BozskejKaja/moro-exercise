/**
 * Created by Filip Drgoň on 12/03/18.
 */

import React, {Component} from 'react';
import PropTypes from 'prop-types';

export default class CreateNewUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: "",
        }
    }

    addUser = () => {
        const {addHandler} = this.props;
        addHandler(this.state.user);
        this.setState({user: ""})
    };

    render() {
        const {user} = this.state;
        return (
            <div className="creation">
                <div className="input-wrapper">
                    <input
                        type="text"
                        value={user}
                        onChange={(e) => this.setState({user: e.target.value})}
                        onKeyDown={e => e.keyCode === 13 && this.addUser()}
                    />
                </div>
                <div className="button-wrapper">
                    <button
                        className="add-button"
                        onClick={this.addUser}
                    >
                        Add user
                    </button>
                </div>
            </div>
        );
    }
}

CreateNewUser.propTypes = {
    addHandler: PropTypes.func,
};
