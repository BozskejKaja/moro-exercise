import React, {Component} from 'react';

import logo from './Bitmap.png';
import './scss/App.css';
import UsersTable from "./components/UsersTable";

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            users: [
                {id: 1, name: "Robert Kalinak"},
                {id: 2, name: "Peter Stasak"},
                {id: 3, name: "Peter Marcin"},
                {id: 4, name: "Miro Noga"},
                {id: 5, name: "Jara Cimrman"},
                {id: 6, name: "Martinko Klingac"},
                {id: 7, name: "Michal Krajcer"},
                {id: 8, name: "Meno Priezvisko"},
                {id: 9, name: "Adam Gonda"},
                {id: 10, name: "Oliver Rydzi"},
                {id: 11, name: "Hung Tran"},
                {id: 12, name: "Filip Obsivac"},
                {id: 13, name: "Marcel Pales"},
                {id: 14, name: "Lojzo Cistic"},
                {id: 15, name: "Jakub Cekovsky"},
            ],
        }
    }

    deleteHandler = (id) => {
        const users = this.state.users.filter(user => user.id !== id);
        this.setState({users});
    };

    editHandler = (editedUser) => {
        const users = this.state.users.map(user => user.id === editedUser.id ? editedUser : user);
        this.setState({users});
    };

    addHandler = (name) => {
        const {users} = this.state;
        const id = users[users.length - 1].id + 1;
        this.setState({users: [...users, {id, name}]})
    };

    render() {
        const {users} = this.state;
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                </header>
                <div className="table-container">
                    <UsersTable
                        users={users}
                        deleteHandler={this.deleteHandler}
                        editHandler={this.editHandler}
                        addHandler={this.addHandler}
                    />
                </div>
            </div>
        );
    }
}

export default App;
